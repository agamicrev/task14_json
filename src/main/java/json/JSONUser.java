package json;

import java.io.File;
import java.util.List;
import model.Device;

public class JSONUser {

  public static void main(String[] args) {
    File json = new File("src/main/resources/deviceJSON.json");
    File schema = new File("src/main/resources/deviceJSONScheme.json");

    JSONParser parser = new JSONParser();

    printList(parser.getBeerList(json));
  }

  private static void printList(List<Device> devices) {
    System.out.println("============ JSON ============");
    for (Device device : devices) {
      System.out.println(device);
      System.out.println("--------------------------------------------------------------");
    }
  }
}
