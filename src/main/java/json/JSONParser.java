package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import model.Device;

public class JSONParser {

  private ObjectMapper objectMapper;

  public JSONParser() {
    this.objectMapper = new ObjectMapper();
  }

  public List<Device> getBeerList(File jsonFile) {
    Device[] devices = new Device[0];
//        List<Device> devices = new ArrayList<>();
    try {
      devices = objectMapper.readValue(jsonFile, Device[].class);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return Arrays.asList(devices);
  }
}
