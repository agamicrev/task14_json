package model;

public class Complection {

  private double interPrice;
  private boolean multimedia;

  public Complection() {
  }

  public Complection(double interPrice, boolean multimedia) {
    this.interPrice = interPrice;
    this.multimedia = multimedia;
  }

  public double getInterPrice() {
    return interPrice;
  }

  public boolean isMultimedia() {
    return multimedia;
  }

  public void setInterPrice(double interPrice) {
    this.interPrice = interPrice;
  }

  public void setMultimedia(boolean multimedia) {
    this.multimedia = multimedia;
  }


  @Override
  public String toString() {
    return "[" +
        "interPrice: " + interPrice +
        ", multimedia: " + multimedia +
        ']';
  }
}

