package model;

import java.util.ArrayList;
import java.util.List;

public class Device {

  private int deviceID;
  private String name;
  private String price;
  private boolean critical;
  private String origin;
  private List<Type> types = new ArrayList<>();
  private Complection complection;

  public Device() {
  }

  public Device(int deviceID, String name, String price, boolean critical, String origin,
      List<Type> types, Complection complection) {
    this.deviceID = deviceID;
    this.name = name;
    this.price = price;
    this.critical = critical;
    this.origin = origin;
    this.types = types;
    this.complection = complection;
  }

  public int getDeviceID() {
    return deviceID;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return price;
  }

  public boolean isCritical() {
    return critical;
  }

  public String getOrigin() {
    return origin;
  }

  public List<Type> getTypes() {
    return types;
  }

  public Complection getComplection() {
    return complection;
  }

  public void setDeviceID(int deviceID) {
    this.deviceID = deviceID;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public void setCritical(boolean critical) {
    this.critical = critical;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public void setTypes(List<Type> types) {
    this.types = types;
  }

  public void addType(Type type) {
    this.types.add(type);
  }

  public void setComplection(Complection complection) {
    this.complection = complection;
  }

  @Override
  public String toString() {
    return "<<< DEVICE >>>" +
        "\ndeviceID: " + deviceID +
        "\nname: '" + name + '\'' +
        "\norigin: '" + origin + '\'' +
        "\nprice: '" + price + '\'' +
        "\ntypes: " + types +
        "\ncomplection: " + complection +
        "\ncritical: " + critical;
  }
}
